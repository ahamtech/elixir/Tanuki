defmodule Tanuki.UsersKeysTest do
  use ExUnit.Case, async: false
  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  @client Tanuki.Client.new(%{private_token: "g8zWsDJqs5LCn9-JRmJh"}, "http://localhost:3000/api/v3/")
  alias Tanuki.Users.Keys

  setup_all do
    ExVCR.Config.cassette_library_dir("test/fixtures/vcr_cassettes")
    HTTPoison.start
  end

  test "Keys.mine/1" do
    use_cassette "keys_mine" do
      assert Keys.mine(@client) |> Enum.empty?
    end
  end

  test "Keys.find/1" do
    use_cassette "keys_find" do
      { :error, response_struct } = Keys.find(2, @client)
      assert response_struct.body.message == "404 Not found" # no keys configured
    end
  end

  test "Keys.create/2" do
    key = %{
      title: "Tanuki",
      key: "ssh-rsa tanuki"
    }
    use_cassette "keys_create" do
      assert Keys.create(@client, key)
    end
  end

  test "Keys.delete/2" do
    use_cassette "keys_delete" do
      refute Keys.delete(4, @client) # Returns nil in the body
    end
  end
end
