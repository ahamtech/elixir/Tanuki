defmodule Tanuki.Users do
  @doc """
  GET /user

  Get your own profile! :simple_smile:
  """
  def me(client), do: Tanuki.get("user", client)

  @doc """
  Get /issues

  Parameters
  - state(optional) 	Return all issues or just those that are opened or closed
  - labels (optional) Comma-separated list of label names
  - order_by (optional) Return requests ordered by created_at or updated_at fields. Default is created_at
  - sort (optional) Return requests sorted in asc or desc order. Default is desc
  """
  def issues(client, params \\ []), do: Tanuki.get("issues", client, params)

  @doc """
  GET /users

  Without params, it lists all users
  call users(client, username: "root") to search for user root (and only user root)
  Pass [{search: term}] to find all users associated with the term
  """
  def list(client, params \\ []), do: Tanuki.get("users", client, params)

  @doc """
  GET /users/:id

  Gets the user by id, response might vary on your own role
  """
  def find(id, client), do: Tanuki.get("users/#{id}", client)

  @doc """
  POST /users

  Creates a new user. Note only administrators can create new users.

  Parameters:
    - email (required) - Email
    - password (required) - Password
    - username (required) - Username
    - name (required) - Name
    - skype (optional) - Skype ID
    - linkedin (optional) - LinkedIn
    - twitter (optional) - Twitter account
    - website_url (optional) - Website URL
    - projects_limit (optional) - Number of projects user can create
    - extern_uid (optional) - External UID
    - provider (optional) - External provider name
    - bio (optional) - User's biography
    - admin (optional) - User is admin - true or false (default)
    - can_create_group (optional) - User can create groups - true or false
    - confirm (optional) - Require confirmation - true (default) or false
  """
  def create(client, params), do: Tanuki.post("users", client, params)

  @doc """
  PUT /users/:id

  Modifies an existing user. Only administrators can change attributes of a user.
  Note, at the moment this method does only return a 404 error, even in cases where a 409 (Conflict) would be more appropriate, e.g. when renaming the email address to some existing one.

  Optional parameters:
    - email - Email
    - username - Usernamective=false
    - skype - Skype ID
    - linkedin - LinkedIn
    - twitter - Twitter account
    - website_url - Website URL
    - projects_limit - Limit projects each user can create
    - extern_uid - External UID
    - provider - External provider name
    - bio - User's biography
    - admin - User is admin - true or false (default)
    - can_create_group - User can create groups - true or false
  """
  def modify(id, params, client), do: Tanuki.put("users/#{id}", client, params)

  @doc """
  DELETE /users/:id

  Deletes a user. Available only for administrators. This is an idempotent function, calling this function for a non-existent user id still returns a status code 200 OK. The JSON response differs if the user was actually deleted or not. In the former the user is returned and in the latter not.
  """
  def delete(id, client), do: Tanuki.delete("users/#{id}", client)

  @doc """
  PUT /users/:uid/block

  Blocks the specified user. Available only for admin.
  """
  def block(user_id, client), do: Tanuki.put("users/#{user_id}/block", client)

  @doc """
  PUT /users/:uid/unblock

  Unblocks the specified user. Available only for admin.
  """
  def unblock(user_id, client), do: Tanuki.put("users/#{user_id}/unblock", client)
end
