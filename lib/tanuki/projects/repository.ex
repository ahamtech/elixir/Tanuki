defmodule Tanuki.Projects.Repositories do
  @doc """
  GET /projects/:id/repository/tree

  Get a list of repository files and directories in a project.

  Parameters:
  - path (optional) - The path inside repository. Used to get contend of subdirectories
  - ref_name (optional) - The name of a repository branch or tag or if not given the default branch
  """
  def tree(id, client, params \\ []), do: Tanuki.get("projects/#{id}/repository/tree", client, params)

  @doc """
  GET /projects/:id/repository/blobs/:sha

  Get the raw file contents for a file by commit SHA and path.

  Parameters:
  - filepath (required) - The path the file
  """
  def blobs(id, sha, client, params), do: Tanuki.get("projects/#{id}/repository/blobs/#{sha}", client, params)

  @doc """
  GET /projects/:id/repository/raw_blobs/:sha

  Get the raw file contents for a blob by blob SHA.
  """
  def raw_blobs(id, sha, client), do: Tanuki.get("projects/#{id}/repository/raw_blobs/#{sha}", client)

  @doc """
  GET /projects/:id/repository/archive

  Get an archive of the repository

  Parameters:
  - sha (optional) - The commit SHA to download defaults to the tip of the default branch
  """
  def archive(id, client, params \\ []), do: Tanuki.get("projects/#{id}/repository/archive", client, params)

  @doc """
  GET /projects/:id/repository/compare

  Compare branches, tags or commits

  Parameters:
  - from (required) - the commit SHA or branch name
  - to (required) - the commit SHA or branch name
  """
  def diff(id, client, params), do: Tanuki.get("projects/#{id}/repository/compare", client, params)

  @doc """
  GET /projects/:id/repository/contributors

  Get repository contributors list
  """
  def contributors(id, client), do: Tanuki.get("projects/#{id}/repository/contributors", client)
end
