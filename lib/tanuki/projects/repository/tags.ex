defmodule Tanuki.Projects.Repository.Tags do
  @doc """
  GET /projects/:id/repository/tags

  Get a list of repository tags from a project, sorted by name in reverse alphabetical order.
  """
  def list(id, client), do: Tanuki.get("projects/#{id}/repository/tags", client)

  @doc """
  POST /projects/:id/repository/tags

  Creates a new tag in the repository that points to the supplied ref.

  Parameters:
  -  tag_name (required) - The name of a tag
  -  ref (required) - Create tag using commit SHA, another tag name, or branch name.
  -  message (optional) - Creates annotated tag.
  -  release_description (optional) - Add release notes to the git tag and store it in the GitLab database.
  """
  def create(id, client, params), do: Tanuki.post("projects/#{id}/repository/tags", client, params)

  @doc """
  POST /projects/:id/repository/tags/:tag_name/release

  Add release notes to the existing git tag. It returns 201 if the release is created successfully. If the tag does not exist, 404 is returned. If there already exists a release for the given tag, 409 is returned.

  Parameters:
  - description (required) - Release notes with markdown support
  """
  def create_release(id, tag_name, client, params), do: Tanuki.post("projects/#{id}/repository/#{tag_name}/release", client, params)

  @doc """
  PUT /projects/:id/repository/tags/:tag_name/release

  Updates the release notes of a given release. It returns 200 if the release is successfully updated. If the tag or the release does not exist, it returns 404 with a proper error message.

  Parameters:
  - description (required) - Release notes with markdown support
  """
  def modify_release(id, tag_name, client, params), do: Tanuki.put("projects/#{id}/tags/#{tag_name}/release", client, params)
end
