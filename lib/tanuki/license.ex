defmodule Tanuki.License do
  @moduledoc """
  Retrieve information about the current license
  """

  @doc """
  In order to retrieve the license information, you need to authenticate yourself as an admin.
  """
  def list(client), do: Tanuki.get("license", client)
end
