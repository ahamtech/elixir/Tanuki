defmodule Tanuki.Users.Emails do
  @doc """
  GET /user/emails

  Get your own authenticated email adresses
  """
  def mine(client), do: Tanuki.get("user/emails", client)

  @doc """
  GET /user/emails/:id

  Gets your email adress by id
  """
  def find(id, client), do: Tanuki.get("user/emails/#{id}", client)

  @doc """
  GET /users/:uid/emails

  Gets the keys for the user with the passed uid
  """
  def list(uid, client), do: Tanuki.get("users/#{uid}/emails", client)

  @doc """
  POST /user/emails

  Required params:
  - email

  Adds a new email adress to your account
  """
  def create(client, params), do: Tanuki.post("user/emails", client, params)

  @doc """
  POST /users/:id/email

  Required params:
  - email

  Adds a new key to the account with the id passed
  """
  def create_for_user(id, params, client), do: Tanuki.post("users/#{id}/emails", client, params)

  @doc """
  DELETE /user/email/:id

  Remove your own email my id
  """
  def delete(id, client), do: Tanuki.delete("user/emails/#{id}", client)

  @doc """
  DELETE /users/:uid/emails/:id

  Remove a email with :id for user with :uid
  """
  def delete_for_user(uid, email_id, client), do: Tanuki.delete("users/#{email_id}/emails/#{uid}", client)
end
