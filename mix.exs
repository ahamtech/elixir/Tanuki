defmodule Tanuki.Mixfile do
  use Mix.Project

  def project do
    [
      app: :tanuki,
      version: "0.2.1",
      elixir: "~> 1.3",
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      description: description(),
      package: package(),
      deps: deps(),
      aliases: aliases()
    ]
  end

  # Configuration for the OTP application
  #
  # Type `mix help compile.app` for more information
  def application do
    [applications: [:logger, :httpoison]]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # Type `mix help deps` for more examples and options
  defp deps do
    [
      {:httpoison, "~> 1.8"},
      # HTTP requests
      {:jason, "~> 1.2"},
      # Docs
      {:earmark, ">= 0.0.0", only: :dev},
      # Docs
      {:ex_doc, "~> 0.10", only: :dev},
      # Doc coverage
      {:inch_ex, "~> 0.5", only: :docs},
      {:excoveralls, "~> 0.4", only: :test}
    ]
  end

  defp package do
    [
      maintainers: ["Zeger-Jan van de Weg"],
      licenses: ["MIT"],
      links: %{
        "Github" => "https://github.com/ZJvandeWeg/Tanuki",
        "GitLab (Mirror)" => "https://gitlab.com/zj/Tanuki"
      }
    ]
  end

  defp description do
    """
    GitLab API wrapper in Elixir, named after GitLabs mascot
    """
  end

  defp aliases do
    [compile: ["compile --warnings-as-errors"]]
  end
end
